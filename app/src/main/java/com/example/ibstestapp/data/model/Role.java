
package com.example.ibstestapp.data.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Role {

    @SerializedName("name")
    private String mName;
    @SerializedName("permissions")
    private List<String> mPermissions;

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public List<String> getPermissions() {
        return mPermissions;
    }

    public void setPermissions(List<String> permissions) {
        mPermissions = permissions;
    }

}
