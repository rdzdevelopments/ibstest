package com.example.ibstestapp.data.retrofit;

import com.example.ibstestapp.data.model.LoginResponse;
import com.example.ibstestapp.data.model.RequestBodyParameter;
import com.example.ibstestapp.data.model.UserResponse;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface IBSapi {

    @POST("auth/v2/oauth/token")
    Observable<LoginResponse> logIn(
            @Query("grant_type") String grant_type,
            @Query("username") String username,
            @Query("password") String password
    );

    @GET("user/v2/account")
    Observable<UserResponse> getUser();
}
