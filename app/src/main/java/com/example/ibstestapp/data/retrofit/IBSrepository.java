package com.example.ibstestapp.data.retrofit;

import com.example.ibstestapp.data.model.LoginResponse;
import com.example.ibstestapp.data.model.RequestBodyParameter;
import com.example.ibstestapp.data.model.UserResponse;

import io.reactivex.Observable;

public class IBSrepository {

    RetrofitClient apiClient = new RetrofitClient();

    public Observable<LoginResponse> logIn(String grantType, String username, String password){
        return apiClient.getIBSApi().logIn(grantType, username, password);
    }

    public Observable<UserResponse> getUser(String token){
        apiClient.setToken(token);
        return apiClient.getIBSApi().getUser();
    }

}
