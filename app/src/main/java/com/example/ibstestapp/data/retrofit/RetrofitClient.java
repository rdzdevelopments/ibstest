package com.example.ibstestapp.data.retrofit;

import androidx.lifecycle.MutableLiveData;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    static final String BASE_URL = "https://coesys.demo.gemalto.com/mbi/";

    private static Retrofit.Builder retrofit;

    OkHttpClient getHttpClient(String token){
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(chain -> {
            Request request = chain.request().newBuilder().addHeader("Authorization", "Bearer " + token).build();
            return chain.proceed(request);
        });
        return httpClient.build();
    }



    private void initRetrofit() {
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create());
    }

    public IBSapi getIBSApi(){
        if (retrofit == null){
            initRetrofit();
        }
        return retrofit.build().create(IBSapi.class);
    }

    public void setToken(String token){
        retrofit.client(getHttpClient(token));
    }





}
