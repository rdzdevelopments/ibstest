
package com.example.ibstestapp.data.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class UserResponse {

    @SerializedName("accountNonExpired")
    private Boolean mAccountNonExpired;
    @SerializedName("accountNonLocked")
    private Boolean mAccountNonLocked;
    @SerializedName("creationDate")
    private String mCreationDate;
    @SerializedName("credentialsNonExpired")
    private Boolean mCredentialsNonExpired;
    @SerializedName("email")
    private String mEmail;
    @SerializedName("enabled")
    private Boolean mEnabled;
    @SerializedName("firstName")
    private String mFirstName;
    @SerializedName("id")
    private Long mId;
    @SerializedName("language")
    private String mLanguage;
    @SerializedName("lastName")
    private String mLastName;
    @SerializedName("location")
    private String mLocation;
    @SerializedName("roles")
    private List<Role> mRoles;
    @SerializedName("username")
    private String mUsername;

    public Boolean getAccountNonExpired() {
        return mAccountNonExpired;
    }

    public void setAccountNonExpired(Boolean accountNonExpired) {
        mAccountNonExpired = accountNonExpired;
    }

    public Boolean getAccountNonLocked() {
        return mAccountNonLocked;
    }

    public void setAccountNonLocked(Boolean accountNonLocked) {
        mAccountNonLocked = accountNonLocked;
    }

    public String getCreationDate() {
        return mCreationDate;
    }

    public void setCreationDate(String creationDate) {
        mCreationDate = creationDate;
    }

    public Boolean getCredentialsNonExpired() {
        return mCredentialsNonExpired;
    }

    public void setCredentialsNonExpired(Boolean credentialsNonExpired) {
        mCredentialsNonExpired = credentialsNonExpired;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public Boolean getEnabled() {
        return mEnabled;
    }

    public void setEnabled(Boolean enabled) {
        mEnabled = enabled;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public void setFirstName(String firstName) {
        mFirstName = firstName;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getLanguage() {
        return mLanguage;
    }

    public void setLanguage(String language) {
        mLanguage = language;
    }

    public String getLastName() {
        return mLastName;
    }

    public void setLastName(String lastName) {
        mLastName = lastName;
    }

    public String getLocation() {
        return mLocation;
    }

    public void setLocation(String location) {
        mLocation = location;
    }

    public String getUsername() {
        return mUsername;
    }

    public void setUsername(String username) {
        mUsername = username;
    }

    public List<Role> getmRoles() {
        return mRoles;
    }

    public void setmRoles(List<Role> mRoles) {
        this.mRoles = mRoles;
    }

}
