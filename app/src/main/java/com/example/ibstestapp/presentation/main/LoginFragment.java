package com.example.ibstestapp.presentation.main;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ibstestapp.data.model.UserResponse;
import com.example.ibstestapp.databinding.LoginFragmentBinding;

public class LoginFragment extends BaseFragment {

    private MainViewModel mViewModel;
    private LoginFragmentBinding binding;


    private Observer<UserResponse> userResponseObserver = userResponse -> {
        if (userResponse != null){
            replaceFragment(UserProfileFragment.newInstance());
        }
    };


    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = LoginFragmentBinding.inflate(inflater, container, false);
        binding.setLifecycleOwner(this);
        binding.setFragment(this);
        binding.setViewmodel(mViewModel);
        return binding.getRoot();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewModel = new ViewModelProvider(getActivity()).get(MainViewModel.class);
    }

    @Override
    public void onStart() {
        super.onStart();
        configObservers();
    }

    @Override
    public void onStop() {
        super.onStop();
        removeObservers();
        resetLoginData();
        closeKeyboard();
    }

    private void configObservers() {
        mViewModel.userResponse.observe(getViewLifecycleOwner(), userResponseObserver);
    }

    private void removeObservers() {
        mViewModel.userResponse.removeObserver(userResponseObserver);
    }

    public void logInButton(){
        mViewModel.logIn(binding.username.getText().toString(), binding.password.getText().toString());
    }

    private void resetLoginData() {
        mViewModel.errorMessage.setValue("");
        binding.username.setText("");
        binding.password.setText("");
    }

    @Override
    void onBackPressed() {
        getActivity().finish();
    }
}