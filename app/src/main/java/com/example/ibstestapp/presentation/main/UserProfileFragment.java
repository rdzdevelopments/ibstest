package com.example.ibstestapp.presentation.main;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.ibstestapp.R;
import com.example.ibstestapp.data.model.UserResponse;
import com.example.ibstestapp.databinding.UserProfileFragmentBinding;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

public class UserProfileFragment extends BaseFragment {

    private MainViewModel mViewModel;
    private UserProfileFragmentBinding binding;


    private Observer<UserResponse> userResponseObserver = userResponse -> {
        if (userResponse == null){
            removeFragment();
        }
    };

    public static UserProfileFragment newInstance() {
        return new UserProfileFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = UserProfileFragmentBinding.inflate(inflater, container, false);
        binding.setLifecycleOwner(this);
        binding.setFragment(this);
        binding.setUserData(mViewModel.userResponse.getValue());
        return binding.getRoot();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewModel = new ViewModelProvider(getActivity()).get(MainViewModel.class);
    }

    @Override
    public void onStart() {
        super.onStart();
        configObservers();
    }

    @Override
    public void onStop() {
        super.onStop();
        removeObservers();
    }

    private void configObservers() {
        mViewModel.userResponse.observe(getViewLifecycleOwner(), userResponseObserver);
    }

    private void removeObservers() {
        mViewModel.userResponse.removeObserver(userResponseObserver);
    }

    private void logOutButton(){
        mViewModel.logOut();
    }

    @Override
    void onBackPressed() {
        showLogOutDialog();
    }

    public void showLogOutDialog() {
        new MaterialAlertDialogBuilder(getContext())
                .setTitle(getString(R.string.logout_title))
                .setPositiveButton(getString(R.string.confirm), (dialogInterface, i) -> {
                    logOutButton();
                })
                .setNegativeButton(getString(R.string.cancel), (dialogInterface, i) -> { })
                .show();
    }

}