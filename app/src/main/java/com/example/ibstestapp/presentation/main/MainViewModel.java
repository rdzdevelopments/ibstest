package com.example.ibstestapp.presentation.main;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.ibstestapp.data.model.LoginResponse;
import com.example.ibstestapp.data.model.UserResponse;
import com.example.ibstestapp.data.retrofit.IBSrepository;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MainViewModel extends ViewModel {

    private IBSrepository ibsRepo = new IBSrepository();
    public MutableLiveData<UserResponse> userResponse = new MutableLiveData<>();
    public MutableLiveData<String> errorMessage = new MutableLiveData<>("");
    public MutableLiveData<Boolean> isLoading = new MutableLiveData<>(false);


    void logIn(String username, String password){
        isLoading.setValue(true);
        ibsRepo.logIn("password", username, password)
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<LoginResponse>() {

                    @Override
                    public void onSubscribe(Disposable d) { }

                    @Override
                    public void onNext(LoginResponse value) {
                        getUser(value.getAccessToken());
                    }

                    @Override
                    public void onError(Throwable e) {
                        isLoading.postValue(false);
                        errorMessage.postValue("User or password incorrect. Try again");
                    }

                    @Override
                    public void onComplete() { }
                });
    }


    void getUser(String token){

        ibsRepo.getUser(token)
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<UserResponse>() {

                    @Override
                    public void onSubscribe(Disposable d) { }

                    @Override
                    public void onNext(UserResponse value) {
                        isLoading.postValue(false);
                        userResponse.postValue(value);
                    }

                    @Override
                    public void onError(Throwable e) {
                        isLoading.postValue(false);
                        errorMessage.postValue("An error ocurred while fetching user data. Try later.");
                    }

                    @Override
                    public void onComplete() { }
                });
    }

    void logOut(){
        userResponse.setValue(null);
    }
}